import json
import requests
import ayar
from client import Client
import talib
import numpy as np

import time
from datetime import datetime
from datetime import timedelta

class Kasif:
    def __init__(self,piyasa):
        self.piyasa = piyasa
        self.durum = "buyuk"
        self.client = None
        self.periyot = Client.KLINE_INTERVAL_1MINUTE
        self.limit = 50
        self.tabloAdi = "Kasif"
        self.baglan()
        self.guncelFiyat = 0.0
        self.sonAlimFiyati = 0.0
        self.sonSatisFiyati = 0.0
        self.karOrani = 1.01
        self.islemTuru = "Alış"
        self.dusmeOrani = 0.999

    def baglan(self):
        try:
            self.client = Client(ayar.binanceKey, ayar.binanceSecret)
        except:
            print("Binance'e bağlanılamadı.")
            time.sleep(3)

    def tara(self):
        while(True):
            try:
                mumlar = self.client.get_klines(symbol=self.piyasa,interval=self.periyot,limit=self.limit)
                kisa = self.ortalama(mumlar,20)
                uzun = self.ortalama(mumlar,50)

                yeniDurum = "buyuk" if kisa > uzun else "kucuk"

                print("+" if yeniDurum=="buyuk" else "-",flush=True,end="")

                if self.durum == "kucuk" and yeniDurum=="buyuk":
                    #Al Sinyali
                    print("AL :",self.piyasa)
                    self.sinyal()
                    time.sleep(30*60)
                self.durum = yeniDurum

            except Exception as e:
                print("HATA :",e)
                time.sleep(3)
            time.sleep(5*60)

    def ortalama(self,dizi,adet):
        son = len(dizi)-1
        ilk = son - adet-1
        toplam = 0.0
        for i in range(ilk,son):
            toplam += float(dizi[i][4])
        return toplam/adet

    def sinyal(self):
        zaman = datetime.utcnow() + timedelta(hours=3)
        zamanText = zaman.strftime("%Y.%m.%d %H:%M:%S")
        data = {"Zaman": zamanText, "Tür": self.islemTuru, "Piyasa": self.piyasa, "Fiyat": self.guncelFiyat}
        veri = json.dumps(data)

        link = ayar.appScriptBase+"?action=insert&table="+self.tabloAdi+"&data="+veri
        while(True):
            try:
                requests.get(link)
                break
            except Exception as e:
                print("HATA :",e)
                self.baglan()

    def tara_kmt(self):

        shortEMA = 0.0  # starting with None
        middleEMA = 0.0  # starting with None
        longEMA = 0.0  # starting with None

        flag_short = False

        # we also need to store the last variables that was the value for the ema_8 and ema_21, so we can compare
        last_short = 0.0
        last_middle = 0.0

        while True:
            self.guncelFiyat = float(self.client.get_symbol_ticker(symbol=self.piyasa)["price"])

            try:
                mumlar = self.client.get_klines(symbol=self.piyasa, interval=self.periyot, limit=self.limit)
                kapanis_fiyatlari = []
                for each in mumlar:
                    kapanis_fiyatlari.append(float(each[4]))

                np_kapanis_fiyatlari = np.array(kapanis_fiyatlari)
                zaman = datetime.utcnow() + timedelta(hours=3)
                zamanText = zaman.strftime("%Y.%m.%d %H:%M:%S")
                shortEMA = talib.EMA(np_kapanis_fiyatlari, 8)[-1]
                middleEMA = talib.EMA(np_kapanis_fiyatlari, 26)[-1]  # same as the last one

                print("Zaman: " + zamanText + " -- Short EMA :", str(shortEMA) + " -- Middle EMA :", str(middleEMA))

                if middleEMA < shortEMA and last_middle > last_short and flag_short is False:
                    # Al Sinyali
                    print("C.AL :", self.piyasa)
                    self.islemTuru = "C.Alış"
                    self.sonAlimFiyati = self.guncelFiyat
                    flag_short = True
                    self.sinyal()
                elif middleEMA < last_middle <= last_short < shortEMA and flag_short is False:
                    # Al Sinyali
                    print("AL :", self.piyasa)
                    self.islemTuru = "Alış"
                    self.sonAlimFiyati = self.guncelFiyat
                    flag_short = True
                    self.sinyal()
                elif flag_short is True and last_short > shortEMA * self.dusmeOrani > middleEMA * self.dusmeOrani and self.guncelFiyat > self.sonAlimFiyati * self.karOrani:
                    # SAT sinyali
                    print("SAT :", self.piyasa)
                    self.islemTuru = "Satış"
                    self.sonSatisFiyati = self.guncelFiyat
                    flag_short = False
                    self.sinyal()

                last_short = shortEMA
                last_middle = middleEMA

            except Exception as e:
                print("HATA KMT :", e)
                time.sleep(3)
            time.sleep(30)

